# beamerGSI

LaTeX Beamer GSI template.

## Getting Started

Download content of this project to **beamerGSI** directory.
Add following lines to your latex file:

```
\usepackage{beamerGSI/beamerthemelocal}
\usefolder{beamerGSI}
\usetheme{GSI}
```

## Using Git submodule

```
git init submodule beamerGSI https://git.gsi.de/p.miedzik/beamerGSI.git
```